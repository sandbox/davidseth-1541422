<?php

/**
 * Expose the active menu tree as a context condition
 */
class context_condition_active_menu_tree extends context_condition {
  function condition_values() {
    $menus = menu_parent_options(array_reverse(menu_get_menus()), NULL);
    $root_menus = array();
    foreach ($menus as $key => $name) {
      $id = explode(':', $key);
      if ($id[1] == '0') {
        $root_menus[$id[0]] = check_plain($name);
      }
      else {
        $link = menu_link_load($id[1]);
        $root_menu = $root_menus[$id[0]];
        $menus[$root_menu][$link['mlid']] = $name;
      }
      unset($menus[$key]);
    }
    array_unshift($menus, "-- ". t('None') ." --");
    return $menus;
  }

  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    $form['#multiple'] = TRUE;
    return $form;
  }

  function execute() {
    $enabled_contexts = context_enabled_contexts();
    foreach ($enabled_contexts as $context) {
      if (isset($context->conditions['active_menu_tree']['values'])) {
        // grab the parents of the menu item at the current path
        $results = db_query("SELECT p1, p2, p3, p4, p5, p6, p7, p8, p9 FROM {menu_links} WHERE link_path=:path", array(':path' => $_GET['q']))->fetchAssoc();

        if ($results == NULL) {
          return;
        }

        $menu_item_array = $results;
        foreach($context->conditions['active_menu_tree']['values'] as $item) {
          $found_key = array_search($item, $menu_item_array);
          if($found_key !== FALSE) {
            $this->condition_met($context, $menu_item_array[$found_key]);
            break;
          }
        }
      }
    }
  }
}